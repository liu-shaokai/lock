package com.nx.arch.lock;

import java.util.concurrent.TimeUnit;

import com.nx.arch.addon.lock.NxLock;
import com.nx.arch.addon.lock.NxLockClient;

public class Test3 {
	 public static void main(String[] args) throws Exception {
	     
	    	final NxLockClient client = NxLock.factory().etcdSolution()
	    								  .connectionString("http://etcd1.Lock.nxinc.com:2379","http://etcd2.Lock.nxinc.com:2379","http://etcd3.Lock.nxinc.com:2379","http://etcd4.Lock.nxinc.com:2379","http://etcd5.Lock.nxinc.com:2379")
	    								  .clusterName("monitor-server")
	    								  .build();
	    	
	    	for(int i = 0 ;i < 100000 ;i++){
	    	    new Thread() {    
	    	        @Override
	    	        public void run() {
	    	            try(NxLock lock = client.newLock("testfirstLock2")){
	    	                TimeUnit.MILLISECONDS.sleep(100);
	                    } catch (Exception e) {
	                    }
	    	        };
	    	    }.start();
	    	    TimeUnit.SECONDS.sleep(1);
	    	}
	    	
	    	
	    	try(NxLock lock = client.newLock("lockName")){
	    	    if(lock.acquire(10)) {
	    	        // 成功获得锁
	    	    }
            }
	    	
	    	
	    }

}
